title: Guix Foundation
date: 2023-09-17 0:00
---
Guix Foundation is a non-profit association according to French law
(known as [_loi 1901_](https://en.wikipedia.org/wiki/Voluntary_association#France)),
[registered in 2016](/statutes/JOAFE_PDF_Unitaire_20160008_00760.pdf)
and with SIRET number 917 723 066 00010.

The mission of Guix Foundation is to support the
[GNU Guix](https://guix.gnu.org/) project and more generally to promote
and develop software and operating systems that respect user freedom.
It [owns](/assets) and hosts part of the infrastructure for the Guix
build farm and helps fund and organise [events](/events).

Guix Foundation accepts donations by wire transfer or during in-person
events, complementing the [FSF’s Working Together
fund](https://my.fsf.org/civicrm/contribute/transact?reset=1&id=50).

The current board is composed of
[Simon Tournier](https://simon.tournier.info) as Presidency and
Tanguy Le Carrour as Treasury.
While well-identified public representatives of the association are
required by French law, our aim is to have a collegiate and
collective governance. In fact, all decisions are taken by the
Solidary Administrative Council, to which all members of the
association wishing to take responsibility for its functioning
can be elected.


To know more about our bylaws, you may consult the
* [Statutes](/statutes/statuts-201602-en.pdf)
  (or their
  [French
  original](/statutes/statuts-202207-fr.pdf))
* [Interior
Reglementary](/statutes/interieur-201602-en.pdf)
  (or its
  [French
original](/statutes/interieur-201602-fr.pdf))

If you wish to join, please fill in the
[membership
form](/statutes/membershipform.txt)
and send it to the e-mail addresses of the Presidency and the Treasury
given above.

