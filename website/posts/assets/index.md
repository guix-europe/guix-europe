title: Assets
date: 2025-03-04 0:00
---
## Finances

Our [financial
status](https://framagit.org/guix-europe/guix-europe/-/blob/main/accounting/accounting.ledger)
is public and permanently updated in a
[ledger](https://www.ledger-cli.org/) file.


## Advertising material

- 2 kakemonoes, one at Andreas's, one with Tanguy at
  [Easter-eggs](https://www.easter-eggs.com/)'s offices.
- `guix.info` domain

## Machines

Guix Europe owns some hardware, usually hosted by members, to take part in
operating the
[bordeaux](https://bordeaux.guix.gnu.org/activity)
and the
[berlin](https://ci.guix.gnu.org/workers)
[build farms](https://qa.guix.gnu.org/branch/master).


### Bayfront

Among other uses, head of the bordeaux build farm, hosted at
[Aquilenet](https://www.aquilenet.fr/).


### Overdrive

6
[Softiron Overdrive
1000](https://github.com/SoftIron/overdrive/tree/master/1000)
- [overdrive1](https://ci.guix.gnu.org/machine/overdrive1)
  hosted by Ludo, berlin build farm
- [lieserl](https://ci.guix.gnu.org/machine/lieserl)
  hosted by Julien, berlin build farm
- formerly dover, hosted by Collin, offline
- formerly monokuma, hosted by Gábor, offline
- sergei hosted by Tobias, offline
- dmitri hosted by Tobias, offline


### Honeycomb

6
[Honeycomb
LX2](https://www.solid-run.com/arm-servers-networking-platforms/honeycomb-servers-workstation/)
boards
- [pankow](https://ci.guix.gnu.org/machine/pankow),
  hosted at MDC, berlin build farm
- [grunewald](https://ci.guix.gnu.org/machine/grunewald),
  hosted at MDC, berlin build farm
- [kreuzberg](https://ci.guix.gnu.org/machine/kreuzberg),
  hosted at MDC, berlin build farm
- hamal,
  hosted by Chris, bordeaux build farm
- hatysa,
  hosted by Chris, bordeaux build farm
- hadar,
  hosted by Chris, bordeaux build farm


### Novena

1 [Novena](https://www.kosagi.com/w/index.php?title=Novena_Main_Page)
board
- formerly redhill and an external hard drive, hosted by Efraim, offline


### Laptops

We have received a donation of several laptops from
[Tweag](https://www.tweag.io/).
Some of them, all Lenovo Thinkpad X1 Gen9, are part of the bordeaux build farm:
- kranji, hosted by Andreas
- marsiling, hosted by Andreas
- tampines, hosted by Andreas
- yishun, hosted by Andreas

Others are with members of the project to help with Guix development;
so far, all of these are also Lenovo Thinkpad X1 Gen9:
- Andreas (bedok)
- Efraim
- Jonathan M.

