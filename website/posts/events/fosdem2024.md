title: GNU Guix Days FOSDEM 2024
date: 2024-02-07 0:00
---
This page is for tracking anything related with the 2024 GNU Guix/Guile
conference and hackathon on February 1st and 2nd — right before
[FOSDEM2024](https://fosdem.org/2024/) and the
[Declarative and Minimalistic Computing
devroom](https://fosdem.org/2024/schedule/track/declarative-and-minimalistic-computing/).

Annual GNU Guix (un)conference. *This event is a*
[FOSDEM 2024 fringe event](https://fosdem.org/2024/fringe/).

## Why?

GNU Guix is growing rapidly and has gone from a software packaging system to a
full tool stack aimed at reproducible software deployment and development.
GNU Guix is a toolkit that allows developers to integrate reproducible software
deployment into their applications—as opposed to leaving it up to the user. GNU
Guix is based on the GNU Guile programming language which makes it a very
versatile and hackable (in the good sense) environment.

## When?

Thursday, February 1st and February 2nd 2024, the two days before FOSDEM.

Coffee at 9:30AM, and starting at 10AM.

## Where?

[Institute of Cultural Affairs (ICAB)](http://icab.be/) Rue Amedee Lynen 8 1210
Brussels
([map](https://www.openstreetmap.org/?mlat=50.85019&mlon=4.37320#map=18/50.85019/4.37320)),
Belgium.

## Programme

We had some talks in the morning and hacking and discussions the
rest of the day. Two days of GNU Guix bliss.

The event targeted an audience of Guix developers and users.

In pure "unconference" style, the rest of the program was made by
participants as we went, with hands-on sessions organised in subgroups.
The following session topics were proposed:

 1. GNU Guix status update & road map
 2. GNU Guix online documentation
 3. Alternative target architectures (ARM, RISCV, POWER etc.)
 4. Project governance & facilitation: RFC process, teams, maintenance, release management
 5. Sustaining our infrastructure: planning to keep qa.guix, data.guix, ci.guix, etc. afloat
 6. Spritely, Guix, and Guile—the ''amazing'' trio!
 7. Next steps for GSoC paramaterized packages
 8. Re-imagining the Guix CLI, now with actual forethought and consistency!

## Presentations and notes

### Day 1

Main track:

- Introduction to Goblins by Christine Lemmer-Webber
- Status of Guix by Efraim Flashner
- Guix QA update by Christopher Baines

Sessions:

- Alt. Arch.
- Bootstrapping
- Documentation
- Gobelin
- Guix Home
- Infrastructure
- Release

### Day 2

Sessions:

- CLI
- Funding
- Governance/Maintainers
- Hoot
- Hurd
- L10N
- Newbie room
- Onboarding
- Patch flow
- Profiling

### Ideas that didn’t make it to sessions

- Shepherd
- HPC

## Code of conduct

Be aware that attendees implicitly abide by the
[code of conduct as stated by FOSDEM](https://fosdem.org/2024/practical/conduct/ ).
As this is a FOSDEM fringe event the organizers can be contacted in case of a
problem.

## Participants

 1. Gabor Boskovits
 2. Janneke Nieuwenhuizen
 3. Ludovic Courtès
 4. Pjotr Prins
 5. Manolis Ragkoukis
 6. Yvan Sraka
 7. Efraim Flashner
 8. Jonathan McHugh
 9. Arun Isaac
10. Wilko Meyer
11. Tanguy Le Carrour
12. Ekaitz Zarraga
13. Fabio Natali
14. Rutger van Beusekom
15. Andreas Enge
16. Edino Tavares Moniz
17. Collin Doering
18. Christopher Baines
19. Christine Lemmer-Webber
20. Robin Templeton
21. Julien Lepiller
22. pukkamustard
23. Alexey Abramov
24. Perelandra
25. Steve George
26. Juliana Sims
27. Jelle Licht
28. Michael Bauer
29. Rostislav Svoboda
30. Jonathan Brielmaier
31. Fernando Ayats
32. Romain Garbage
33. Samuel Matthiesen
34. Picnoir
35. Ramses de Norre
36. Simon Tournier
37. Gwen
38. NLNet rep.
39. Arjan Adriaanse
40. Ricardo Wurmus
41. Lio Novelli
42. Jurij Podgoršek
43. Tobias Platen
44. Sven Plaga
45. Guido
46. Adriel Dumas
47. Danny Milosavljevic
48. Adam McCartney

## Costs

Attendance is free.  We will ask a voluntary contribution for consumptions.
