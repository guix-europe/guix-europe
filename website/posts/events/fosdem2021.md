title: GNU Guix Day FOSDEM 2021
date: 2022-11-13 0:00
---
This page was for tracking anything related with the 2021 GNU Guix/Guile
online conference and hackathon on February 8th — right after
[FOSDEM 2021](https://fosdem.org/2021/) and the
[Declarative and Minimalistic Computing Languages
devroom](https://fosdem.org/2021/schedule/track/declarative_and_minimalistic_computing)
and the
original application for a [declarative and minimalistic language
devroom](https://libreplanet.org/wiki/FOSDEM2021-devroom-proposal).

Annual GNU Guix (un)conference. This event was online and a [FOSDEM 2021
fringe event](https://fosdem.org/2021/fringe/).

## Why?

GNU Guix is growing rapidly and has gone from a software packaging
system to a full tool stack aimed at reproducible software deployment
and development. GNU Guix is a toolkit that allows developers to
integrate reproducible software deployment into their applications — as
opposed to leaving it up to the user. GNU Guix is based on the GNU Guile
programming language which makes it a very versatile and hackable (in
the good sense) environment.

## When?

Monday, February 8th, 2021, the day after FOSDEM.

Starting at 5am UTC (6am Amsterdam, 7am Athens) we covered over 12 hours
to cater for Asia, Europe and the USA time zones.

## Where?

Online using a free software conferencing system.

## Programme

We had some talks in the mid session (morning EU) and hacking and
discussions the rest of the day. A day of GNU Guix hacking bliss.

The event targeted an audience of Guix developers and users.

In the European morning (from 9AM UTC+1) the following sessions were
planned:

1.  **Quality Assurance (QA)**, hosted by Christopher Baines
2.  **Bootstrapping: next step**, hosted by Jan Nieuwenhuizen
3.  **Continuous integration: Cuirass news and look forward**, hosted by
    Mathieu Othacehe around 12:30 UTC.
4.  **Rust packaging in GNU Guix: How can we do better?**, hosted by
    Hartmut & Efraim (shared pad at
    <https://pubcryptpad.pep.foundation/code/#/2/code/edit/giUFbGwDeEgBGE8OS+Th3GuP/>)

In the European afternoon (from 2PM UTC+1) the following sessions were
planned:

1.  **Outreachy**, hosted by Magali Lemes, Gábor Boskovits, Simon
    Tournier

In pure "unconference" style, the rest of the program was made by
participants as we went, with hands-on sessions organised in subgroups.
Possible session topics that had been proposed included:

1.  GNU Guix road map
2.  GNU Guix online documentation
3.  Mes and bootstrapping with Jan
4.  Reproducibility project
5.  Alternative target architectures (ARM, RISCV, POWER etc.)
6.  Developing workflows (GNU Guix and GNU Workflow Language)
7.  Demonstration and explanation of [The Perfect
    Setup](https://www.gnu.org/software/guix/manual/en/guix.html#The-Perfect-Setup)
    (Emacs, Geiser, Magit)
8.  Demonstration and live-hacking of [Next
    browser](https://next.atlas.engineer) + introduction to the code
    base
9.  Better build systems for JVM languages in GNU Guix (support Maven,
    sbt, Gradle, \...)
10. Reproducible JDK
11. The Guix System installer
12. Improve PHP
13. RISC-V port
14. E-Mail server
15. Demonstration and explanation of
    [guile-netlink](https://git.lepiller.eu/guile-netlink)
16. Presentation of the new translation process
17. your topic?

## Code of conduct

Attendees implicitly abode by the [code of conduct as stated by
FOSDEM](https://fosdem.org/2021/practical/conduct/). As this was a
FOSDEM fringe event the organisers could be contacted in case of a
problem.

## Participants

1.  Ludovic Courtès
2.  Pjotr Prins
3.  Manolis Ragkousis
4.  Efraim Flashner
5.  janneke (Jan) Nieuwenhuizen
6.  Hartmut Goebel
7.  Bonface Munyoki
8.  Christopher Baines
9.  Vagrant Cascadian
10. Jonathan Brielmaier
11. Paul Jewell
12. Andreas Enge
13. Gábor Boskovits
14. Ioanna Dimitriou
15. Marco van Hulten
16. Tanguy Le Carrour
17. Chris Marusich
18. Nguyễn Gia Phong
19. Mathieu Othacehe
20. Simon Tournier
21. Raghav (RG) Gururajan
22. Todor (kondor) Kondić

## Costs

Attendance was free.

