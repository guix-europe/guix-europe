title: Events
---

* [Guix Days 2024](/events/fosdem2024.html)
* [Guix Days 2023](/events/fosdem2023.html)
* [Guix Days 2022](/events/fosdem2022.html)
* [Guix Days 2021](/events/fosdem2021.html)
* [Guix Days 2020](/events/fosdem2020.html)
* [Guix Days 2019](/events/fosdem2019.html)
* [Guix Days 2018](/events/fosdem2018.html)

