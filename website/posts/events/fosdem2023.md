title: GNU Guix Days FOSDEM 2023
date: 2023-06-11 0:00
---
This page was for tracking anything related with the 2023 GNU Guix/Guile
conference and hackathon on February 2nd and 3rd — right before
[FOSDEM2023](https://fosdem.org/2023/) and the
[Declarative and Minimalistic Computing
devroom](https://fosdem.org/2023/schedule/track/declarative_and_minimalistic_computing/).

Annual GNU Guix (un)conference. This event was a [FOSDEM 2023
fringe event](https://fosdem.org/2023/fringe/).

## Why?

GNU Guix is growing rapidly and has gone from a software packaging
system to a full tool stack aimed at reproducible software deployment
and development. GNU Guix is a toolkit that allows developers to
integrate reproducible software deployment into their applications — as
opposed to leaving it up to the user. GNU Guix is based on the GNU Guile
programming language which makes it a very versatile and hackable (in
the good sense) environment.

## When?

Thursday 2nd and Friday 3rd of February, 2023, the two days before
FOSDEM.

Starting at 9:30AM.

## Where?

[Institute of Cultural Affairs (ICAB)](http://icab.be/) Rue Amedee Lynen
8 1210 Brussels
([map](https://www.openstreetmap.org/?mlat=50.85019&mlon=4.37320#map=18/50.85019/4.37320)),
Belgium.

## Programme

We had some talks in the morning and hacking and discussions the
rest of the day. Two days of GNU Guix bliss.

The event targeted an audience of Guix developers and users.

In pure "unconference" style, the rest of the program was made by
participants as we went, with hands-on sessions organised in subgroups.
The following session topics were proposed:

 1. GNU Guix road map
 2. GNU Guix online documentation
 3. Reproducibility project
 4. Alternative target architectures (ARM, RISCV, POWER etc.)
 5. The Guix System installer
 6. Developing workflows (GNU Guix and GNU Workflow Language)
 7. Demonstration and explanation of The Perfect Setup (Emacs, Geiser, Magit)
 8. Python team maintenance and developer session
 9. Security issues (CVE-2021-27851, similar bugs, the big picture, etc)
10. P2P substitutes
11. Guix in "air gapped" environments

## Presentations and notes

* [State of Guix 2023](/downloads/20230203-state-of-guix.org),
  presentation by Ludovic Courtès
* [Debugging Guix beyond pk](/downloads/20230203-debugging.org),
  presentation by Ludovic Courtès and jgart
* [Quality assurance](/downloads/20230203-qa1.txt),
  presentation and notes by Christopher Baines
* [Patch review](/downloads/20230203-qa2.txt),
  notes by Christopher Baines
* [Releases, branches and
  teams](/downloads/20230203-releases-branches.txt),
  notes by Andreas Enge
* [Skill sharing on the perfect setup](https://gitlab.com/zimoun/my-conf),
  configuration presented by Simon Tournier

## Code of conduct

Attendees implicitly abode by the [code of conduct as
stated by FOSDEM](https://fosdem.org/2023/practical/conduct/).

## Participants

 1. Pjotr Prins
 2. Manolis Ragkousis
 3. Simon Tournier
 4. Julien Lepiller
 5. Todor Kondić
 6. Andreas Enge
 7. Bonface Munoki
 8. Efraim Flashner
 9. Jonathan McHugh
10. Arun Isaac
11. Tanguy Le Carrour
12. Alexey Abramov
13. Ludovic Courtès
14. Leo Famulari
15. Christopher Baines
16. Mekeor Melire
17. Lars-Dominik Braun
18. Josselin Poiret
19. Tobias Geerinckx-Rice
20. Jonathan Brielmaier
21. jgart
22. Björn Höfling
23. Mathieu Othacehe
24. Nicolò Balzarotti
25. Gábor Boskovits
26. Jelle Licht
27. Michael Bauer
28. pukkamustard
29. Timo Wilken
30. Janneke Nieuwenhuizen
31. Samuel Fadel
32. Nicolas Roelandt
33. Raffael / sepi
34. Sharlatan Hellseher
35. Bertrand Mathelier
36. Petr Hodina
37. Ivan Vilata i Balaguer

## Costs

Attendance was free. We asked a voluntary contribution for
consumptions.

