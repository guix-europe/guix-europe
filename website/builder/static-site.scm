;;; Haunt --- Static site generator for GNU Guile
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;; Copyright © 2016 Christopher Allan Webber <cwebber@dustycloud.org>
;;; Copyright © 2017, 2020, 2022, 2023 Andreas Enge <andreas@enge.fr>
;;;
;;; This file is part of Haunt.
;;;
;;; Haunt is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Haunt is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Haunt.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Page builders
;;
;;; Code:

(define-module (builder static-site)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt artifact)
  #:use-module (haunt html)
  #:export (picture
            section
            chapter
            sitemap
            theme
            
            make-url
            make-image-url
            chapter-section-post-slug

            static-site))


(define-record-type <picture>
  (make-picture filename caption)
  picture?
  (filename picture-filename)
  (caption picture-caption))

(define* (picture #:key
                  filename
                  (caption #f))
   (make-picture filename caption))

(define-record-type <section>
  (make-section name title)
  section?
  (name section-name)
  (title section-title))

(define* (section #:key
                  (name "Section")
                  (title "Untitled"))
  (make-section name title))

(define-record-type <chapter>
  (make-chapter name title sections)
  chapter?
  (name chapter-name)
  (title chapter-title)
  (sections chapter-sections))

(define* (chapter #:key
                  (name "Chapter")
                  (title "Untitled")
                  (sections '()))
  (make-chapter name title sections)) 

(define-record-type <sitemap>
  (make-sitemap name title shorttitle chapters)
  sitemap?
  (name sitemap-name)
  (title sitemap-title)
  (shorttitle sitemap-shorttitle)
  (chapters sitemap-chapters))

(define* (sitemap #:key
                  (name "Sitemap")
                  (title "Untitled")
                  (shorttitle "Untitled")
                  (chapters '()))
  (make-sitemap name title shorttitle chapters))


(define (make-url webroot relative-url)
   (string-append
     "/" webroot
     (if (equal? webroot "") "" "/")
     relative-url))

(define (make-image-url webroot image)
   (make-url webroot (string-append "images/" image)))

(define (filename-chapter filename)
  "Given FILENAME, extract its substring designating the associated
chapter, that is, the leaf directory."
  (basename (dirname filename)))

(define (filename-section filename)
  "Given FILENAME, extract its substring designating the associated
section, that is its non-directory part without the file extension."
    (string-drop-right (string-trim-right
                         (basename filename)
                         (lambda (c)
                           (not (eq? c #\.))))
                       1))

(define (chapter-section-post-slug post)
  "Transform the file-name of POST into a url slug composed of its chapter
and its section part."
  (let ((filename (post-file-name post)))
    (string-append
      (filename-chapter filename)
      "/"
      (filename-section filename))))


(define (section-url webroot chapter section)
  (make-url webroot
            (string-append
              (chapter-name chapter) "/"
              (section-name section) ".html")))

(define (search-chapter sitemap name)
   "Return the chapter object inside SITEMAP with name NAME."
   (list-ref (filter (lambda (chapter)
                       (string=?
                         (chapter-name chapter)
                         name))
                     (sitemap-chapters sitemap))
             0))

(define (render-section-dropdown webroot chapter current-section-name)
   "Create the dropdown menu with the sections for chapter CHAPTER
on a page of section CURRENT-SECTION-NAME."
  `(ul (@ (class "submenu dropdown-menu"))
    ,@(map
      (lambda (section)
        `((li (@ (class
                   ,(string-append "nav-item"
                                   (if (string=? (section-name section)
                                                 current-section-name)
                                       " section-active" " section-inactive"))))
            (a (@ (class "nav-link")
                  (href ,(section-url webroot chapter section)))
               ,(section-title section)))))
      (chapter-sections chapter))))


(define (render-chapter-menu-item webroot chapter
   current-chapter-name current-section-name)
   "Create a menu item for CHAPTER on a page of chapter named
CURRENT-CHAPTER-NAME and section named CURRENT-SECTION-NAME."
  (let ((has-submenu? (> (length (chapter-sections chapter)) 1)))
    `((li (@ (class
               ,(if (string=? (chapter-name chapter)
                              current-chapter-name)
                    "chapter-active" "chapter-inactive")))
          ,(if has-submenu?
               `(details
                 (@ (class "dropdown"))
                 (summary ,(chapter-title chapter))
                 ,(render-section-dropdown webroot chapter current-section-name))
               `(a (@ (href ,(section-url webroot
                                          chapter
                                          (list-ref (chapter-sections chapter) 0))))
                   ,(chapter-title chapter)))))))


(define %logo
  '(a (@ (href "/"))
      (img (@ (src "/assets/logo.svg")
              (alt "Guix Foundation logo")) "")))

(define (render-chapter-menu webroot sitemap current-chapter current-section)
  "Create a menu showing the chapters in SITEMAP on a page of chapter
CURRENT-CHAPTER and section CURRENT-SECTION."
  `(nav (@ (class "container"))
        (ul (li ,%logo))
        (ul
         ,@(map
            (lambda (chapter)
              (render-chapter-menu-item webroot chapter
                                        current-chapter current-section))
            ;; Drop the first chapter from the menu.  That one is
            ;; assumed to be the home page, which---by convention---is
            ;; supposed to be accessed via the logo.
            (cdr (sitemap-chapters sitemap))))))


(define-record-type <theme>
  (make-theme name layout post-template)
  theme?
  (name theme-name)
  (layout theme-layout)
  (post-template theme-post-template))

(define (default-layout site webroot sitemap language logo carousel
                        current-chapter-name current-section-name
                        css js title body)
  `((doctype "html")
    (html (@ (lang language))
      (head
        (meta (@ (charset "utf-8")))
        (meta (@ (name "viewport")
                 (content "width=device-width, initial-scale=1, shrink-to-fit=no")))
        ,@(map (lambda (file)
                 `(link (@ (rel "stylesheet")
                           (type "text/css")
                           (href ,(make-url webroot
                                   (string-append "assets/css/" file))))))
               css)
        ,@(map (lambda (file)
                 `(script (@ (src ,(make-url webroot
                                    (string-append "assets/js/" file))))))
               js)
        (title ,(string-append title " — " (site-title site))))
      (body
       ,(render-chapter-menu webroot sitemap
                             current-chapter-name current-section-name)
       (header
        (@ (id "header"))
        (h1 ,title))
       (main (@ (id "content") (class "container"))
             ,body)
       (footer
        (section
         (@ (class "container"))
         (p "© 2024 Guix Foundation.")
         (dl
          (dt "SIRET number")
          (dd "917 723 066 00010")
          (dt "Presidency")
          (dd (a (@ (href "https://simon.tournier.info")) "Simon Tournier"))
          (dt "Treasury")
          (dd "Tanguy Le Carrour"))))))))

(define* (make-post-template #:key
                             (language "en"))
  (lambda (post)
    `(,(post-sxml post)
      (div (@ (class "last-modification"))
           (a (@ (href "https://framagit.org/guix-europe/guix-europe"))
              "Source code of this site.")))))

(define* (theme #:key
                (name "Theme")
                (layout default-layout)
                (language "en")
                (post-template (make-post-template #:language language)))
  (make-theme name layout post-template))

(define (with-layout theme site webroot sitemap language logo carousel
                     current-chapter-name current-section-name
                     css js title body)
  ((theme-layout theme) site webroot sitemap language logo carousel
                        current-chapter-name current-section-name
                        css js title body))


(define* (static-site #:key
                      sitemap
                      (webroot "")
                      (language "en")
                      (logo #f)
                      (css '())
                      (js '())
                      (carousel #f)
                      (theme (theme #:language language)))
  "Return a procedure that transforms a list of posts into pages decorated
by THEME; the structure of the site is passed via SITEMAP, a list of css
files via CSS and a list of javascript files via JS.
WEBROOT indicates where the site is hosted with respect to the domain,
excluding surrounding slahes."

  (lambda (site posts)

    (define (post->page post)
      (let* ((filename (string-append (site-post-slug site post)
                                       ".html"))
             (chapter-name (filename-chapter (post-file-name post)))
             (section-name (filename-section (post-file-name post)))
             (title (post-ref post 'title))
             (body ((theme-post-template theme) post)))
        (serialized-artifact filename
          (with-layout theme site webroot sitemap language logo carousel
                       chapter-name section-name
                       css js title body)
          sxml->html)))

    (define (search-post chapter section)
       (list-ref (filter (lambda (post)
                           (and (string=?
                                  (filename-chapter (post-file-name post))
                                  chapter)
                                (string=?
                                  (filename-section (post-file-name post))
                                  section)))
                         posts)
                 0))

    ;; As index.html, use the first section of the first chapter in the
    ;; sitemap.
    (define index-page
      (let* ((filename "index.html")
             (chapter (list-ref (sitemap-chapters sitemap) 0))
             (section (list-ref (chapter-sections chapter) 0))
             (chapter-name (chapter-name chapter))
             (section-name (section-name section))
             (title (section-title section))
             (post (search-post chapter-name section-name))
             (body ((theme-post-template theme) post)))
        (serialized-artifact filename
          (with-layout theme site webroot sitemap language logo carousel
                       chapter-name section-name
                       css js title body)
          sxml->html)))

    (append (map post->page posts)
            (list index-page))))

