Minutes of the Solidary Administrative Council meeting of 2025-02-07

Present:
- Andreas Enge
- Adriel Dumas-Jondeau
- Efraim Flashner
- Indieterminacy
- Oliver Probst
- Simon Tournier
- Tanguy Le Carrour

1) Membership request

The membership request from Jean Simard is accepted unanimously.
