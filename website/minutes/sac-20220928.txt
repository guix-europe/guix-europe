Minutes of the Solidary Administrative Council meeting of 2022-09-28


Present: Andreas Enge, Julien Lepiller, Simon Tournier, Ricardo Wurmus,
Pjotr Prins


1) Membership requests

The membership request by Adriel Dumas-Jondeau is accepted unanimously.
It is also decided to accept potentiel further membership requests by
persons having attended the 10 Years event in Paris.

