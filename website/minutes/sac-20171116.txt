Minutes of the Solidary Administrative Council meeting of 2017-11-16


Present: Ludovic Courtès, Andreas Enge


1) Membership requests

The membership requests by Christopher Baines and Ricardo Wurmus are accepted
unanimously.


2) Expenses

It is decided unanimously that Andreas be reimbursed for the import tax
for the Redhill machine of 77€40, paid in December 2015, and for a MicroSD
card (5€49) paid in March 2017 to replace the broken one in the machine.

It is decided unanimously that Guix Europe bears the shipping fees to send
the bayfront server back to its supplier for an exchange.

