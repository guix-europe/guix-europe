Compte rendu de l' Assemblée Générale Ordinaire du 2020-06-21

Présents:
Andreas Enge (Trésorerie), Christopher Baines, Efraim Flashner,
Julien Lepiller, Jonathan Brielmaier, Ludovic Courtès, Manolis Ragkousis
(Présidence), Oliver Propst, Pjotr Prins, Ricardo Wurmus, Simon Tournier;
11 sur 19 adhérents, le quorum de 2/3 des adhérents pour modifier les
statuts n'est pas atteint


1) Rapport d'activité par la Présidence

   Manolis présente les activités depuis la dernière Assemblée Générale
   en décembre 2017.


2) Rapport financier par la Trésorerie
   
   Un rapport avec l'état des finances a été envoyé par Andreas par
   courrier électronique avant l'assemblée; il n'y a pas de questions
   supplémentaires. L'association dispose de 2824€84 pour financer
   ses activités.


3) Élection de la Présidence et de la Trésorerie

   Manolis est réélu à la Présidence, Andreas à la Trésorerie; Christopher
   est élu comme adjoint à la Trésorerie. Les votes sont unanimes.


4) Élection du Collège d'Administration Solidaire (CAS)

   Tous les membres actuels du CAS sont présents, et tous les adhérents
   présents ont envie de rejoindre le CAS. Ils sont élus à l'unanimité.


5) Vote sur les changements aux Statuts et au Règlement intérieur proposés
   par Simon Tournier

   Les modifications ne concernent que des erreurs typographiques, néanmoins
   le quorum des 2/3 des adhérents n'étant pas atteint, un vote ne peut avoir
   lieu.


6) Activités futures

   L'assemblée discute des activités pour 2020 et 2021, notamment la tenue
   des Guix Days autour du FOSDEM à Bruxelles 2021, ainsi que des événements
   en ligne fin 2020.

   Du soutien financier notamment pour des étudiants GSoC et Outreachy
   pour participer à nos événements est également discuté.

   En fin de compte, nous débattons sur l'utilité d'une nouvelle initiative
   de levée de fonds.

