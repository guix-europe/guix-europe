Compte-rendu de l'assemblé générale ordinaire du 6 novembre 2021

Présents à la réunion en ligne sur BigBlueButton:
Andreas Enge, Efraim Flashner, Julien Lepiller, Manolis Ragkousis,
Oliver Propst, Pjotr Prins, Simon Tournier.
Cela représente 7 membres sur 19, le quorum des 2/3 n'est pas atteint.


1) Rapport moral du président

   Manolis présente les activités suivantes de Guix Europe qui ont eu
   lieu depuis la dernière assemblé générale en juin 2020.

   * Les mini journées Guix « Guix Days » à la fin de l'année 2020
   * La tenue d'une devroom au FOSDEM en ligne ; nous avons eu des
     intervenants qui ne participent d'habitude pas, et environ 60
     participants
   * Version en ligne des journées Guix juste après le FOSDEM, environ
     70 participants


2) Rapport financier du trésorier

   Andreas commente les comptes visibles dans le livre de compte public
   sur
   https://git.savannah.gnu.org/cgit/guix/maintenance.git/tree/guix-europe/accounting/accounting.ledger
   La plupart de l'argent disponible pour les activités liées à Guix
   est tenu par la FSF ; les seules transactions financières de Guix
   Europe depuis la dernière assemblée générale ont été l'avance des
   fonds pour l'hébergement de serveurs, qui sont remboursés environ
   une ou deux fois par an par la FSF ; l'encaissement des
   cotisations ; le paiement de notre cotisation à Aquilenet et le
   paiement pour cinq ans du domaine guix.info. Le solde de nos comptes
   s'élève à près de 3000 €.


3) Élections du nouveau président et trésorier

   Manolis souhaite laisser sa place après presque quatre ans de
   présidence ; nous exprimons notre gratitude pour tout son travail
   pour la promotion de Guix et Guix Europe et le prenons au mot
   pour la co-organisation de futurs événements. Les personnes
   suivantes se présentent pour l'élection :
   - Président: Simon Tournier
   - Vice-président: Oliver Propst
   - Trésorier: Andreas Enge
   - Vice-trésorier: Julien Lepiller
   Les quatre candidats sur ces postes sont élus à l'unanimité.


4) Élection du Collège d'Administration Solidaire (CAS)
   En tant que membres du Collège d'Administration Solidaire,
   l'assemblé générale élit à l'unanimité les quatre personnes déjà
   mentionnées au point 3) ainsi que les trois autres membres présents,
   Efraim Flashner, Manolis Ragkousis et Pjotr Prins.

   Le Collège peut accueillir plus de membres ; nous décidons de
   solliciter plus de candidatures qui seront examinées lors de notre
   prochain assemblée générale.

5) Bourse de déplacement

   Nous pourrions proposer des bourses de déplacement quand les
   rencontres en présentiels recommenceront. Une petite quantité
   d'argent pourrait venir de Guix Europe, et une plus grosse partie
   pourrait venir des fonds tenus par la FSF si le commité financier
   l'accepte.

6) Collecte de fonds

  Simon suggère de chercher à faire en sorte que Guix Europe devienne
  une association d'intérêt général pour pouvoir bénéficier de
  déductions d'impôts en France, ce qui pourrait attirer des dons. En
  fonction de la législation locale, les déductions d'impôts pourraient
  être possibles dans d'autres pays européens, en particulier pour les
  entreprises.


7) Possibilité de futures rencontres Guix physiques ou numériques

   * Le FOSDEM 2022 se tiendra en ligne les 5 et 6 février 2022.
     Julien, Manolis et Pjotr sont volontaires pour organiser la
     devroom de Guix ; la date limite de candidature est le 15
     novembre.
   * Les « Guix Days » après le FOSDEM. Nous pourrions organiser une
     rencontre physique éventuellement pendant le printemps, en
     supposant que la situation se calme d'ici là par rapport au Covid.

8) Activités futures

   * Nous avons besoin d'une page web, par exemple sur
     association.guix.info. Oliver et Simon sont volontaires pour
     travailler sur ce point.
   * Nous pourrions avoir besoin d'un logo, mais avons besoin d'aide
     pour cela.
   * Oliver suggère d'écrire un billet pour promouvoir l'adhésion à
     Guix Europe.
   * Oliver suggère d'écrire un bille pour promouvoir Guix en dehors de
     sa communauté actuelle.
   * Les histoires de déploiements réussis pourraient aussi trouver leur
     place sur le blog, et sont d'habitude très peu couvertes par le
     blog de Guix.
   * Il est nécessaire d'administrer et d'héberger du matériel. Les
     membres peuvent se porter volontaire pour cela.
   * Nous parlons de plusieurs idées pour changer de nom, pour le
     rendre moins centré sur l'Europe, même si l'association continuera
     d'être enregistrée en France. Les suggestions sont : Guix Together
     (c'est déjà le nom d'un événement à Miami, nous devrons leur
     demander la permission ; Simon se charge de leur envoyer un
     courriel). Mais « Guix Europe » a aussi ses soutiens. Nous
     devrions en discuter sur la liste de diffusion.
   * Nous devrions vérifier si nous pouvons ouvrir un compte bancaire
     chez Wise et accepter les cotisation en d'autres devises que
     l'euro.

