# Accounting

French accounting is based on codified "account" names. For instance,
`5:3:1` is for the cash box. The codes are hierarchical. `5:3:1` is a sub-class
of `5` which contains all the financial accounts.

Most humans don’t know what to do with those codes, so we use aliases and
notes to be able to print useful informations. See <includes/accounts.ledger>
and <includes/legal.ledger>.

To instruct the `ledger` command to display human readable information, some
environment variables have to be set. See `LEDGER_*_FORMAT` in the <.env> file.

Once loaded (using `source .env` or Direnv’s `dotenv .env` function), we can
start asking questions using the `ledger` command.

**Warning:** it’s not yet possible to use aliases and notes to query the ledger!
See the `scripts/` folder for useful wrappers.


## Commands

### Cash box and cheques

```
# Amount in the cash box
$ ledger bal 5:3

# Cheques to be cashed
$ ledger bal 5:1:1:2

# Cheques transactions in 2025
$ ledger print 5:1:1:2 -p 2025

# All the available money
$ ledger bal ^5
           2463.65 €  5 Comptes financiers
           2443.65 €    1:2:1 Comptes en monnaie nationale
           2319.65 €      1 Compte courant Banque Populaire
            124.00 €      2 Compte courant SumUp
             20.00 €    3:1 Caisse
--------------------
           2463.65 €
```


### Membership fees

```
# Membership fees for 2025
$ ledger print 7:5:6:1 --by-payee -p 2025 \
  | grep 2025 | grep -v prepayments | cut -d " " -f 2- | sort

# Is a member up to date of their membership fee for 2025
$ ledger reg --empty 7:5:6:1 and @Member-UUID -p 2025 \
  | wc -l

# All the membership fees of a member
$ ledger reg --empty 7:5:6:1 and @Member-UUID \
  | cut -d " " -f 1

# All the membership fee history of a member
$ ledger reg --empty "(4:8:7:0 or 7:5:6:1) and @Member-UUID"

# Check if all the prepayments have been processed
$ ledger bal 4:8:7:0
```


### Loans to reimburse

```
$ ledger bal ^1:6
$ ledger print ^1:6

```


## Resources

Mostly in French!

- [Cours de comptabilité des associations 1901 / 1905](https://calebgestion.com/cours_comptabilite/index.html)
- [L'enregistrement comptable des dons et des cotisations reçus](https://calebgestion.com/cours_comptabilite/c32_enregistrement_comptable_cotisations_dons.htm)
- [Comptabiliser les cotisations dans une association](https://www.compta-online.com/comptabiliser-les-cotisations-dans-une-association-ao1692)
- [Nomenclature du bilan comptable associatif](https://www.legalplace.fr/guides/plan-comptable-association/)
- [Découvrir et comprendre le Plan Comptable des Associations](https://www.assoconnect.com/blog/articles/23661-plan-comptable-des-associations-modele-a-telecharger-et-conseils/)
- [Explications de la classe 6](https://www.indy.fr/guide/tenue-comptable/plan-comptable/compte-classe-six/)
- [Ledger Cheatsheet](https://devhints.io/ledger)
- [Ledger pour les asso’](https://ontoblogie.clabaut.net/posts/202406/ledger-pour-la-compta-dune-asso.html)
- [Plan comptable](https://www.associatheque.fr/fr/fichiers/bao/modele-document-plan-comptable.pdf)
- [La comptabilité associative](https://www.associations.gouv.fr/la-comptabilite-associative.html)
- [Plain Text Accounting (PTA)](https://plaintextaccounting.org/)
- [Vim plugin for Ledger](https://github.com/ledger/vim-ledger)
- [Synchronize your ledger-cli files with your bank](https://github.com/egh/ledger-autosync)
