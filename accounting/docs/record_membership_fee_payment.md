# Recording a membership fee payment

For a payment in cash for the current year (2025):

```legder
# postings/2025.ledger
2025-01-01 * A useful description
   Membership fees                 €-10 ; Payee: Member-UUID
   Cash box
```

For a prepayment over the wire in 2025 for 2026:

```legder
# postings/2025.ledger
2025-01-01 * A useful description
   Prepayments on membership fees  €-10 ; Payee: Member-UUID
   SumUp

# postings/2026.ledger
2026-01-01 * Processing of prepayments
   Membership fees                 €-10 ; Payee: Member-UUID
   Prepayments on membership fees
```
