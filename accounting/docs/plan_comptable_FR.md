# Plan comptable des associations

## Classe 1 : Comptes de capitaux

Cette classe comprend les fonds propres, emprunts et dettes assimilées.

10. Fonds propres et réserves
11. Report à nouveau
12. Résultat de l’exercice
13. Subventions d’investissement
15. Provisions pour risques et charges
16. Emprunts et dettes assimilées
18. Comptes de liaison des établissements
19. Projet associatif et fonds dédiés


## Classe 2 : Comptes d’immobilisations

Il s’agit notamment de tout ce qui concerne les installations, terrains et
matériel de base.

20. Immobilisations incorporelles.
21. Immobilisations corporelles.
23. Immobilisations en cours.
26. Participations et créances rattachées à des participations.
27. Autres immobilisations financières.
28. Amortissements des immobilisations.
29. Provisions pour dépréciation des immobilisations


## Classe 3 : Comptes de Stocks et en-cours

Comme son nom l’indique, cette classe comprend les produits, matières et travaux
qui sont utilisés pendant une période non permanente.

31. Matières premières et fournitures
32. Autres approvisionnements.
33. En-cours de production de biens.
34. En-cours de production de services.
35. Stocks de produits.
37. Stocks de marchandises.
39. Provisions pour dépréciation des stocks et en-cours.


## Classe 4 : Comptes de Tiers

Cette classe regroupe les comptes concernant tous les acteurs externes, qu’il
s’agisse de fournisseurs, clients, créanciers, débiteurs…

40. Fournisseurs et comptes rattachés.
41. Usagers et comptes rattachés.
42. Personnel et comptes rattachés.
43. Sécurité sociale et autres organismes sociaux.
44. État et autres collectivités publiques.
45. Confédération, fédération, unions, associations affiliées.
  455. Associés - Comptes courants
46. Débiteurs divers et créditeurs divers.
  467. Dirigeants
  4681. Frais des bénévoles
47. Comptes transitoires ou d’attente.
48. Comptes de régularisation.
49. Provisions pour dépréciation des comptes de tiers.


## Classe 5 : Comptes Financiers

Cette classe comprend les comptes bancaires mais aussi les actions et
obligations.

50. Valeurs mobilières de placement.
51. Banques, établissements financiers et assimilés.
  5112. Chèques à encaisser
  5121. Comptes bancaires
53. Caisse.
54. Régies d’avances et accréditifs.
58. Virements internes.
59. Provisions pour dépréciation des comptes financiers.


## Classe 6 : Comptes de Charges

Cette classe compte les charges, c’est-à-dire les différents types de dépenses
faites par l’association.

60. Achats (sauf 603, correspondant à la variation des stocks).
61. Services extérieurs.
62. Autres services extérieurs.
63. Impôts, taxes et versements assimilés.
64. Charges de personnel.
65. Autres charges de gestion courante.
66. Charges financières.
67. Charges exceptionnelles.
68. Dotations aux amortissements et aux provisions.
69. Impôts sur les bénéfices et assimilés.


## Classe 7 : Comptes de Produits

Cette classe compte les produits, c’est-à-dire l’argent généré par l’activité de
l’association.

70. Ventes de produits finis, prestations de services, marchandises.
71. Production stockée (ou destockage).
72. Production immobilisée.
74. Subventions d’exploitation.
75. Autres produits de gestion courante.
  754. Ressources liées à la générosité du public
    7541. Dons manuels
      75411. Dons manuels
        754110. Dons manuels non affectés
        754111. Dons manuels affectés
          7541111. Dons manuels affectés au projet "1"
      75412. Abandons de frais par les bénévoles
    756. Cotisations
      7561. Cotisations sans contrepartie
      7562. Cotisations avec contrepartie
76. Produits financiers.
77. Produits exceptionnels.
78. Reprises sur amortissements, dépréciations et provisions.
79. Transferts de charges.


## Classe 8 : Comptes spéciaux

86. Engagements donnés par l’association.
87. Engagements reçus par l’association.
